package com.klg.laststraw

import android.content.Context
import com.klg.last_kitchen.data.Repository
import com.klg.last_kitchen.data.local.LocalDataSource
import com.klg.last_kitchen.data.remote.RemoteDataSource
import com.klg.laststraw.data.local.PreferenceDataSource

/**
 * Injection
 */
object Injection {
    fun provideLastStrawRepository(context: Context) = Repository.getInstance(
            PreferenceDataSource.with(context),
            RemoteDataSource.getInstance(),
            LocalDataSource.getInstance())
}