package com.klg.laststraw

import android.annotation.SuppressLint
import android.app.Application
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.support.annotation.VisibleForTesting
import com.klg.last_kitchen.data.Repository
import com.klg.laststraw.entry.auth.AuthEmailViewModel
import com.klg.laststraw.entry.auth.AuthPassViewModel
import com.klg.laststraw.splash.SplashViewModel
import com.klg.laststraw.training.TrainingViewModel

/**
 * View model factory
 */
class ViewModelFactory private constructor(
    private val application: Application,
    private val lastStrawRepository: Repository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel> create(modelClass: Class<T>) =
        with(modelClass) {
            when {
                isAssignableFrom(SplashViewModel::class.java) ->
                    SplashViewModel(lastStrawRepository)

                isAssignableFrom(TrainingViewModel::class.java) ->
                    TrainingViewModel(lastStrawRepository)

                isAssignableFrom(AuthEmailViewModel::class.java) ->
                    AuthEmailViewModel(lastStrawRepository)

                isAssignableFrom(AuthPassViewModel::class.java) ->
                    AuthPassViewModel(lastStrawRepository)


                else ->
                    throw IllegalArgumentException(application.getString(R.string.unknown_view_model) + modelClass.name)
            }
        } as T

    companion object {

        @SuppressLint("StaticFieldLeak")
        @Volatile
        private var INSTANCE: ViewModelFactory? = null

        fun getInstance(application: Application) =
            INSTANCE ?: synchronized(ViewModelFactory::class.java) {
                INSTANCE ?: ViewModelFactory(
                    application,
                    Injection.provideLastStrawRepository(application.applicationContext)
                )
                    .also { INSTANCE = it }
            }


        @VisibleForTesting
        fun destroyInstance() {
            INSTANCE = null
        }
    }
}