package com.klg.laststraw.training.elements

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.klg.laststraw.R

class TrainingQrCodeFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_training_qr_code, container, false)
    }

    companion object {

        fun newInstance(): TrainingQrCodeFragment {
            return TrainingQrCodeFragment()
        }
    }
}
