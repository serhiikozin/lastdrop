package com.klg.laststraw.training

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.klg.laststraw.training.elements.TrainingDrinkFragment
import com.klg.laststraw.training.elements.TrainingQrCodeFragment
import com.klg.laststraw.training.elements.TrainingStarFragment

/**
 * Training pager adapter
 */
class TrainingPagerAdapter(fm: FragmentManager?) : FragmentPagerAdapter(fm) {

    private var fragments: List<Fragment>? = addFragmentsToSwipe()

    override fun getItem(position: Int): Fragment {
        return fragments!![position]
    }

    override fun getCount(): Int {
        return fragments!!.size
    }

    private fun addFragmentsToSwipe(): List<Fragment> {
        return listOf(TrainingQrCodeFragment.newInstance(),
                TrainingStarFragment.newInstance(),
                TrainingDrinkFragment.newInstance())
    }
}