package com.klg.laststraw.training

import android.arch.lifecycle.Observer
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.merhold.extensiblepageindicator.ExtensiblePageIndicator
import android.content.Intent
import android.support.v4.view.ViewPager
import android.view.View
import android.widget.Button
import com.klg.laststraw.R
import com.klg.laststraw.entry.EntryActivity
import com.klg.laststraw.home.HomeActivity
import com.klg.laststraw.util.obtainViewModel

class TrainingActivity : AppCompatActivity(), ViewPager.OnPageChangeListener, View.OnClickListener {

    private var buttonSkip: Button? = null
    private var buttonNext: Button? = null
    private var viewPager: ViewPager? = null

    private val COUNT_PAGE = 3
    private val STEP = 1

    private lateinit var viewModel: TrainingViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_training)
        init()
        updateViewDependencies()
        viewModel = obtainViewModel()
        viewModel.openLiveDataAuth.observe(this, Observer { openAuthActivity() })
        viewModel.openLiveDataHome.observe(this, Observer { openHomeActivity() })
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.button_next -> onClickNext()
            R.id.button_skip -> onClickSkip()
        }
    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

    }

    override fun onPageSelected(position: Int) {
        pageSelected(position)
    }

    override fun onPageScrollStateChanged(state: Int) {

    }

    private fun init() {
        buttonSkip = findViewById(R.id.button_skip)
        buttonSkip?.setOnClickListener(this)
        buttonNext = findViewById(R.id.button_next)
        buttonNext?.setOnClickListener(this)
        viewPager = findViewById(R.id.view_pager)
    }

    private fun obtainViewModel(): TrainingViewModel =
        obtainViewModel(TrainingViewModel::class.java)

    private fun openHomeActivity() {
        startActivity(Intent(this, HomeActivity::class.java))
        finish()
    }

    private fun openAuthActivity() {
        startActivity(Intent(this, EntryActivity::class.java))
        finish()
    }

    private fun onClickNext() {
        val current = getItem(+STEP)
        if (current < COUNT_PAGE) {
            viewPager?.currentItem = current
        } else {
            viewModel.addTrainingDone()
        }
    }

    private fun onClickSkip() {
        viewModel.addTrainingDone()
    }

    private fun updateViewDependencies() {
        val pagerAdapter = TrainingPagerAdapter(supportFragmentManager)
        viewPager?.adapter = pagerAdapter
        viewPager?.addOnPageChangeListener(this)
        val pagerIndicator = findViewById<ExtensiblePageIndicator>(R.id.page_indicator)
        pagerIndicator.initViewPager(viewPager)
    }

    private fun getItem(i: Int): Int {
        return viewPager!!.currentItem + i
    }

    private fun pageSelected(position: Int) {
        if (position == COUNT_PAGE - STEP) {
            buttonNext!!.text = getString(R.string.start)
            buttonSkip!!.visibility = View.GONE
        } else {
            buttonNext!!.text = getString(R.string.next)
            buttonSkip!!.visibility = View.VISIBLE
        }
    }

}

