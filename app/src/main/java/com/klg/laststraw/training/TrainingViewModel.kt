package com.klg.laststraw.training

import com.klg.last_kitchen.data.Repository
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel


/**
 * Training model view
 */
class TrainingViewModel(private var repository: Repository) :
    ViewModel() {

    val openLiveDataAuth = MutableLiveData<Boolean>()
    val openLiveDataHome = MutableLiveData<Boolean>()

    fun addTrainingDone() {
        repository.isTrainingCompleted = true
        if (!repository.isAuthorisation) {
            openLiveDataAuth.value = true
        } else {
            openLiveDataHome.value = true
        }
    }
}