package com.klg.laststraw.home

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.klg.laststraw.R

class HomeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
    }
}
