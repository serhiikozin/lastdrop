package com.klg.laststraw.entry.auth

import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.klg.laststraw.R
import com.klg.laststraw.entry.EntryActivity

class AuthEmailFragment : Fragment(), View.OnClickListener {

    private lateinit var activity: EntryActivity
    private lateinit var viewModel: AuthEmailViewModel


    override fun onAttach(context: Context?) {
        super.onAttach(context)
        activity = context as EntryActivity
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view = inflater.inflate(R.layout.fragment_auth_email, container, false)
        init(view)
        viewModel = obtainViewModel()
        return view
    }

    override fun onClick(v: View?) {
        activity.findFragment(this, true)
    }

    private fun obtainViewModel(): AuthEmailViewModel =
        ViewModelProviders.of(this).get(AuthEmailViewModel::class.java)


    private fun init(view: View?) {
        if (view != null) {
            view.findViewById<Button>(R.id.button_email)?.setOnClickListener(this)
        }
    }

    companion object {

        fun newInstance(): AuthEmailFragment {
            return AuthEmailFragment()
        }
    }
}
