package com.klg.laststraw.entry.auth

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.klg.laststraw.R

class AuthPassFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view = inflater.inflate(R.layout.fragment_auth_password, container, false)
        init(view)
        return view
    }

    private fun init(view: View?) {

    }

    companion object {

        fun newInstance(): AuthPassFragment {
            return AuthPassFragment()
        }
    }
}
