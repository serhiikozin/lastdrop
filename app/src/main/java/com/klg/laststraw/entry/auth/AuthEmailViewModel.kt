package com.klg.laststraw.entry.auth

import android.arch.lifecycle.ViewModel
import com.klg.last_kitchen.data.Repository

/**
 * Auth email view model
 */
class AuthEmailViewModel(private var repository: Repository) : ViewModel() {

    fun checkEmail(email: String) {
    repository.checkEmail(email)
    }
}