package com.klg.laststraw.entry

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.klg.laststraw.R

class AuthMethodFragment : Fragment(), View.OnClickListener {

    private lateinit var activity: EntryActivity


    override fun onAttach(context: Context?) {
        super.onAttach(context)
        activity = context as EntryActivity
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view = inflater.inflate(R.layout.fragment_auth_method, container, false)
        init(view)
        var viewmodel = activity.obtainViewModel(AuthMethodViewModel::class.java)
        return view
    }

    override fun onClick(v: View?) {
        if (v != null) {
            when (v.id) {
                R.id.button_email_login -> activity.findFragment(this, true)
                R.id.button_facebook_login -> loginFacebook()
                R.id.button_google_login -> loginGoogle()
            }
        }
    }

    private fun loginGoogle() {
    }

    private fun loginFacebook() {
    }

    private fun init(view: View?) {
        if (view != null) {
            view.findViewById<Button>(R.id.button_email_login).setOnClickListener(this)
            view.findViewById<Button>(R.id.button_facebook_login).setOnClickListener(this)
            view.findViewById<Button>(R.id.button_google_login).setOnClickListener(this)
        }
    }

    companion object {

        fun newInstance(): AuthMethodFragment {
            return AuthMethodFragment()
        }
    }
}
