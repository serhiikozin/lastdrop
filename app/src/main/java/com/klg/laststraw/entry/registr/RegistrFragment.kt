package com.klg.laststraw.entry.registr

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.klg.laststraw.R
import android.widget.ToggleButton
import android.widget.RadioGroup


class RegistrFragment : Fragment() {

    private var textViewRegistrThrough: TextView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view: View = inflater.inflate(R.layout.fragment_registration, container, false)
        init(view)
        onToggle(view)
        return view
    }

    val ToggleListener: RadioGroup.OnCheckedChangeListener =
        RadioGroup.OnCheckedChangeListener { radioGroup, i ->
            for (j in 0 until radioGroup.childCount) {
                val view = radioGroup.getChildAt(j) as ToggleButton
                view.isChecked = view.id == i
            }
        }

    fun onToggle(view: View) {
        (view.parent as RadioGroup).check(view.id)
    }

    private fun init(view: View) {
        textViewRegistrThrough = view.findViewById(R.id.text_view_registr_through)
        (view.findViewById<RadioGroup>(R.id.radio_group_toggle))
            .setOnCheckedChangeListener(ToggleListener)
    }

    companion object {

        fun newInstance(): RegistrFragment {
            return RegistrFragment()
        }
    }
}
