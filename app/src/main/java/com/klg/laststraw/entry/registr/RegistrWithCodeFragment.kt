package com.klg.laststraw.entry.registr


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.klg.laststraw.R


/**
 * A simple [Fragment] subclass.
 */
class RegistrWithCodeFragment : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_registration_with_code, container, false)
    }

    companion object {

        fun newInstance(): RegistrWithCodeFragment {
            return RegistrWithCodeFragment()
        }
    }
}
