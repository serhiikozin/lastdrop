package com.klg.laststraw.entry

import android.arch.lifecycle.ViewModel
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentTransaction
import com.klg.laststraw.R
import com.klg.laststraw.entry.auth.AuthEmailFragment
import com.klg.laststraw.entry.auth.AuthEmailViewModel
import com.klg.laststraw.entry.auth.AuthPassFragment
import com.klg.laststraw.entry.forgot.ForgotPassFragment
import com.klg.laststraw.entry.registr.RegistrFragment
import com.klg.laststraw.entry.registr.RegistrWithCodeFragment
import com.klg.laststraw.splash.SplashViewModel
import com.klg.laststraw.util.obtainViewModel

class EntryActivity : AppCompatActivity() {

    var fragmentManager: FragmentManager? = null
    var fragmentTransaction: FragmentTransaction? = null
    val TAG_FRAGMENT: String = "fragment"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_auth)
        init()
        addFragment(AuthMethodFragment())
    }

    private fun init() {
        fragmentManager = supportFragmentManager
    }

    fun findFragment(fragment: Fragment, flag: Boolean) {
        val fragment = when (fragment) {
            is AuthMethodFragment -> {
                AuthEmailFragment()
            }
            is AuthEmailFragment -> {
                if (flag) AuthPassFragment() else RegistrFragment()
            }
            is RegistrFragment -> RegistrWithCodeFragment()
            is AuthPassFragment -> ForgotPassFragment()
            else -> AuthMethodFragment()
        }
        addFragment(fragment)
    }

    fun <T> obtainViewModel(tClass: T): T = obtainViewModel(tClass)


    private fun addFragment(fragment: Fragment) {
        fragmentTransaction = fragmentManager?.beginTransaction()
        fragmentTransaction?.replace(R.id.frame_auth, fragment, TAG_FRAGMENT)
        fragmentTransaction?.addToBackStack(null)
        fragmentTransaction?.commit()
    }

    override fun onBackPressed() {
        fragmentTransaction = fragmentManager?.beginTransaction()
        val fragment = fragmentManager?.findFragmentById(R.id.frame_auth)
        if (fragment != null) {
            fragmentManager?.popBackStack()
        } else {
            super.onBackPressed()
        }

    }
}
