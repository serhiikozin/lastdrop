package com.klg.last_kitchen.data.remote

import android.support.annotation.VisibleForTesting
import com.klg.last_kitchen.data.DataSourceAuth
import com.klg.laststraw.data.model.BaseResponse
import com.klg.laststraw.data.model.Token
import com.klg.laststraw.data.model.User
import com.klg.laststraw.data.remote.retrofit.ApiManager
import io.reactivex.Single

/**
 * Last straw remote data source
 */
class RemoteDataSource private constructor() : DataSourceAuth {

    private val clientId: Int = 2
    private val clientSecret: String = "baseSecret"

    override fun checkEmail(email: String): Single<BaseResponse<String>> {
        return ApiManager.create().checkEmail(clientId, clientSecret, email)
    }

    override fun forgotPassword(email: String): Single<BaseResponse<String>> {
        return ApiManager.create().forgotPassword(clientId, clientSecret, email)
    }

    override fun registration(user: User, password: String): Single<BaseResponse<Token>> {
        return ApiManager.create().registration(
            clientId, clientSecret,
            user.name, user.surname, user.gender, user.email, password
        )
    }

    override fun registrationWithCode(user: User, password: String): Single<BaseResponse<Token>> {
        return ApiManager.create().registrationWithCode(
            clientId, clientSecret,
            user.name, user.surname, user.gender, user.code, user.email, password
        )
    }

    override fun authorisation(email: String, password: String): Single<BaseResponse<Token>> {
        return ApiManager.create().authorisation(clientId, clientSecret, email, password)
    }

    companion object {
        private var INSTANCE: RemoteDataSource? = null

        @JvmStatic
        fun getInstance(): RemoteDataSource {
            if (INSTANCE == null) {
                synchronized(RemoteDataSource::javaClass) {
                    INSTANCE = RemoteDataSource()
                }
            }
            return INSTANCE!!
        }

        @VisibleForTesting
        fun clearInstance() {
            INSTANCE = null
        }
    }

}