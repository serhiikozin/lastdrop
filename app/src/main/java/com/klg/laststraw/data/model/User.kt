package com.klg.laststraw.data.model

/**
 * User information
 */
class User(
    email: String = "",
    name: String = "",
    surname: String = "",
    gender: Boolean = false,
    code: String = ""
) {

    var email: String = ""
    var name: String = ""
    var surname: String = ""
    var gender: Boolean = false
    var code: String = ""

}