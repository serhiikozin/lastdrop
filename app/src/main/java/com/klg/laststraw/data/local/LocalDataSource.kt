package com.klg.last_kitchen.data.local

import android.support.annotation.VisibleForTesting
import com.klg.last_kitchen.data.DataSourceAuth
import com.klg.laststraw.App
import com.klg.laststraw.data.model.BaseResponse
import com.klg.laststraw.data.model.Token
import com.klg.laststraw.data.model.User
import io.reactivex.Single

/**
 * Last straw local data source
 */
class LocalDataSource private constructor() : DataSourceAuth {

    override fun checkEmail(email: String): Single<BaseResponse<String>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun forgotPassword(email: String): Single<BaseResponse<Any>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


    override fun registration(user: User, password: String): Single<BaseResponse<Token>> {
        return throw IllegalArgumentException()
    }

    override fun authorisation(email: String, password: String): Single<BaseResponse<Token>> {
        return throw IllegalArgumentException()
    }

    companion object {
        private var INSTANCE: LocalDataSource? = null

        @JvmStatic
        fun getInstance(): LocalDataSource {
            if (INSTANCE == null) {
                synchronized(LocalDataSource::javaClass) {
                    INSTANCE = LocalDataSource()
                }
            }
            return INSTANCE!!
        }

        @VisibleForTesting
        fun clearInstance() {
            INSTANCE = null
        }
    }
}