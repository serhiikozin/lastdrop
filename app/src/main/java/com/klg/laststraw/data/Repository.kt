package com.klg.last_kitchen.data

import com.klg.laststraw.data.DataSourcePreference
import com.klg.laststraw.data.model.BaseResponse
import com.klg.laststraw.data.model.Token
import com.klg.laststraw.data.model.User
import io.reactivex.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


/**
 * Kitchen repository
 */
class Repository(
    val prefDataSource: DataSourcePreference,
    val remoteDataSource: DataSourceAuth,
    val localDataSource: DataSourceAuth
    ) : DataSourceAuth, DataSourcePreference {

    override var emailCheck: String
        get() = prefDataSource.emailCheck
        set(value) {
            prefDataSource.emailCheck = value
        }

    override var isAuthorisation: Boolean
        get() = prefDataSource.isAuthorisation
        set(value) {
            prefDataSource.isAuthorisation = value
        }

    override var isTrainingCompleted: Boolean
        get() = prefDataSource.isTrainingCompleted
        set(value) {
            prefDataSource.isTrainingCompleted = value
        }

    override fun forgotPassword(email: String): Single<BaseResponse<String>> {
        return forgotPassword(email)
    }

    override fun checkEmail(email: String): Single<BaseResponse<String>> {
        var e: String = prefDataSource.emailCheck
        if (e.equals(email)) {
            return Single.just(BaseResponse(status = true, data = e, error = ""))
        }
        return remoteDataSource.checkEmail(email)
            .doOnSuccess { base -> if (base.status) prefDataSource.emailCheck = base.data }
            .compose(activtySingleConverter())

    }

    override fun authorisation(email: String, password: String): Single<BaseResponse<Token>> {
        return remoteDataSource.authorisation(email, password)
            .compose(activtySingleConverter())
    }

    override fun registration(user: User, password: String): Single<BaseResponse<Token>> {
        return remoteDataSource.registration(user, password)
    }

    override fun registrationWithCode(user: User, password: String): Single<BaseResponse<Token>> {
        return remoteDataSource.registrationWithCode(user, password)
            .compose(activtySingleConverter())
    }

    fun <T> activityListConverter(): ObservableTransformer<T, T> {
        return ObservableTransformer { ob ->
            ob.subscribeOn(Schedulers.io())
        }
    }

    fun <T> activtySingleConverter(): SingleTransformer<T, T> {
        return SingleTransformer { ob ->
            ob.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
        }
    }

    companion object {
        private var INSTANCE: Repository? = null

        /**
         * Returns the single instance of this class, creating it if necessary.

         * @param remoteDataSource the backend data source
         * *
         * @param localDataSource  the device storage data source
         * *
         * @return the [Repository] instance
         */
        @JvmStatic
        fun getInstance(
            preferences: DataSourcePreference,
            remoteDataSource: DataSourceAuth,
            localDataSource: DataSourceAuth
        ) =
            INSTANCE ?: synchronized(Repository::class.java) {
                INSTANCE ?: Repository(preferences, remoteDataSource, localDataSource)
                    .also { INSTANCE = it }
            }


        /**
         * Used to force [getInstance] to create a new instance
         * next time it's called.
         */
        @JvmStatic
        fun destroyInstance() {
            INSTANCE = null
        }
    }
}
