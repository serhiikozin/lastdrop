package com.klg.laststraw.data.remote.retrofit

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import com.klg.laststraw.data.model.BaseResponse
import com.klg.laststraw.data.model.Token
import io.reactivex.Single
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

/**
 * ApiManager
 */
interface ApiManager {

    @FormUrlEncoded
    @POST("user/register")
    fun registration(
        @Header("client-id") clientId: Int,
        @Header("client-secret") clientSecret: String,
        @Field("name") userName: String,
        @Field("surname") userSurname: String,
        @Field("gender") gender: Boolean,
        @Field("email") email: String,
        @Field("password") password: String
    ): Single<BaseResponse<Token>>

    @FormUrlEncoded
    @POST("user/register_code")
    fun registrationWithCode(
        @Header("client-id") clientId: Int,
        @Header("client-secret") clientSecret: String,
        @Field("name") userName: String,
        @Field("surname") userSurname: String,
        @Field("gender") gender: Boolean,
        @Field("code") code: String,
        @Field("email") email: String,
        @Field("password") password: String
    ): Single<BaseResponse<Token>>

    @FormUrlEncoded
    @POST("user/auth")
    fun authorisation(
        @Header("client-id") clientId: Int,
        @Header("client-secret") clientSecret: String,
        @Field("email") email: String,
        @Field("password") password: String
    ): Single<BaseResponse<Token>>

    @FormUrlEncoded
    @POST("user/auth/email")
    fun checkEmail(
        @Header("client-id") clientId: Int,
        @Header("client-secret") clientSecret: String,
        @Field("email") email: String
    ): Single<BaseResponse<String>>

    @FormUrlEncoded
    @POST("user/auth/forgot_password")
    fun forgotPassword(
        @Header("client-id") clientId: Int,
        @Header("client-secret") clientSecret: String,
        @Field("email") email: String
    ): Single<BaseResponse<String>>

    /**
     * Companion object to create the ApiManager
     */
    companion object Factory {

        private val BASE_URL = "http://base/"

        fun create(): ApiManager {
            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BASE_URL)
                .build()

            return retrofit.create(ApiManager::class.java)
        }
    }
}