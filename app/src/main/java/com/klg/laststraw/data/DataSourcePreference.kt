package com.klg.laststraw.data

/**
 * Data source preference
 */
interface DataSourcePreference {

    var isAuthorisation: Boolean
    var isTrainingCompleted: Boolean
    var emailCheck: String
}