package com.klg.laststraw.data.local

import android.content.Context
import android.content.SharedPreferences
import com.klg.laststraw.data.DataSourcePreference
import com.securepreferences.SecurePreferences

/**
 * SettingUser
 */
object PreferenceDataSource : DataSourcePreference {

    private var prefs: SharedPreferences? = null
    private val AUTHORISATION = "authorisation"
    private val TRAINING = "training"
    private val EMAIL = "email"



    fun with(context: Context): PreferenceDataSource {
        prefs = SecurePreferences(context)
        return this
    }

    override var isAuthorisation: Boolean
        get() = prefs!!.getBoolean(AUTHORISATION, false)
        set(value) = prefs!!.edit().putBoolean(AUTHORISATION, value).apply()

    override var isTrainingCompleted: Boolean
        get() = prefs!!.getBoolean(TRAINING, false)
        set(value) = prefs!!.edit().putBoolean(TRAINING, value).apply()

    override var emailCheck: String
        get() = prefs!!.getString(EMAIL, "")
        set(value) = prefs!!.edit().putString(EMAIL, value).apply()

}