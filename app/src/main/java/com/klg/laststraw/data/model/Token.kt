package com.klg.laststraw.data.model


import com.google.gson.annotations.SerializedName

class Token(@SerializedName("error") var error: String,
            @SerializedName("token_type") var tokenType: String,
            @SerializedName("expires_in") var expiresIn: Int,
            @SerializedName("access_token") var accessToken: String,
            @SerializedName("refresh_token") var refreshToken: String)
