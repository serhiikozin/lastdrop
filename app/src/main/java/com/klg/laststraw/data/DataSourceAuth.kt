package com.klg.last_kitchen.data

import com.klg.laststraw.data.model.BaseResponse
import com.klg.laststraw.data.model.Token
import com.klg.laststraw.data.model.User
import io.reactivex.Single

/**
 * Last straw interface data source
 */
interface DataSourceAuth {

    fun checkEmail(email: String): Single<BaseResponse<String>>

    fun forgotPassword(email: String): Single<BaseResponse<String>>

    fun authorisation(email: String, password: String): Single<BaseResponse<Token>>

    fun registration(user: User, password: String): Single<BaseResponse<Token>>
    fun registrationWithCode(user: User, password: String): Single<BaseResponse<Token>>
}