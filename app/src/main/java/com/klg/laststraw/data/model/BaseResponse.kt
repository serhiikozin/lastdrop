package com.klg.laststraw.data.model

import com.google.gson.annotations.SerializedName

/**
 *
 */

class BaseResponse<T>(@SerializedName("status") var status: Boolean,
                      @SerializedName("data") var data: T,
                      @SerializedName("error_code") var error: String)
