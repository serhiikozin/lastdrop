package com.klg.laststraw.splash

import android.arch.lifecycle.Observer
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle

import com.klg.laststraw.R
import com.klg.laststraw.entry.EntryActivity
import com.klg.laststraw.home.HomeActivity

import com.klg.laststraw.util.obtainViewModel
import com.klg.laststraw.training.TrainingActivity

class SplashActivity : AppCompatActivity() {

    private lateinit var viewModel: SplashViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        viewModel = obtainViewModel()
        viewModel.checkingWindowForOpening()
        viewModel.authLiveData.observe(this, Observer { openAuthActivity() })
        viewModel.trainLiveData.observe(this, Observer { openTrainingActivity() })
        viewModel.homeLiveData.observe(this, Observer { openHomeActivity() })
    }

    private fun obtainViewModel(): SplashViewModel = obtainViewModel(SplashViewModel::class.java)

    private fun openTrainingActivity() {
        startActivity(Intent(this, TrainingActivity::class.java))
        finish()
    }

    private fun openAuthActivity() {
        startActivity(Intent(this, EntryActivity::class.java))
        finish()
    }

    private fun openHomeActivity() {
        startActivity(Intent(this, HomeActivity::class.java))
        finish()
    }
}
