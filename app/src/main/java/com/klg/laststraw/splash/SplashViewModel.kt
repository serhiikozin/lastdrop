package com.klg.laststraw.splash

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.klg.last_kitchen.data.Repository

/**
 * Splash view model does a test for a workout or authorization
 */
class SplashViewModel(private var repository: Repository) :
    ViewModel() {

    val authLiveData = MutableLiveData<Boolean>()
    val trainLiveData = MutableLiveData<Boolean>()
    val homeLiveData = MutableLiveData<Boolean>()

    fun checkingWindowForOpening() {
        if (!repository.isTrainingCompleted) {
            trainLiveData.value = true
        } else if (!repository.isAuthorisation) {
            authLiveData.value = true
        } else {
            homeLiveData.value = true
        }
    }
}
