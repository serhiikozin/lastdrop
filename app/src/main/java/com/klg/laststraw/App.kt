package com.klg.laststraw

import android.app.Application

/**
 * Application
 */
class App : Application() {
    companion object;

    override fun onCreate() {
        super.onCreate()
    }
}